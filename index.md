#### Sommaire
1) [RMl (Rust Markup Langage)](#rml-rust-markup-langage)
   1) [Analyseur syntaxique (PEST)](#analyseur-syntaxique-pest)
   2) [Moteur de rendu](#moteur-de-rendu)
2) [Description de la syntaxe](#description-de-la-syntaxe)
   1) [En-tête](#en-tête)
   2) [Corp](#corp)
      1) [Texte](#texte)
      2) [Lien](#lien)
      3) [Image](#image)
      4) [Liste](#liste)
      5) [Tableau](#tableau)
      6) [Balises génériques](#balises-génériques)
   3) [Attributs](#attributs)
# RML (Rust Markup Langage)
**RML** a pour but d'être un langage de **balisage**.\
**RML** s'inspire fortement du **HTML**.\
La syntaxe de **RML** se rapproche fortement de celle du **HTML**.
## Analyseur syntaxique (PEST)
L'analyser de la structure des fichiers **RML** se fait avec un [parser packrat](https://fr.wikipedia.org/wiki/Parser_packrat).\
La mise en place du **parser packrat** se fait avec la crate [pest](https://docs.rs/pest/latest/pest/).\
Les fichiers `pest` permettent de contenir les définitions de grammaire.

Ci-joint une description de la [crate `pest`](https://gitlab.com/cours32/pest/-/blob/master/index.md) (avec des exemples, est en français et se base sur cette [documentation](https://pest.rs/book/)).\
Le [site officiel](https://pest.rs/) de la crate `pest`.
## Moteur de rendu
Actuellement, la crate [à voire la quelle prendre]() permet de mettre de générer l'interface graphique.\
La génération de l'interface graphique se fait une fois que le fichier **RML** a été analysé.
# Description de la syntaxe
Le langage **RML** est constitué de 2 types de balises :
* **balise jumelle :** constitué d'une balise **ouvrante** et d'une balise **fermante** (par exemple : `<head>...</head>`)
* **balise orpheline :** la balise est toute seule et est automatiquement fermé par `/>` (par exemple : `<h1 ... />`)

Une balise peut avoir des attributs (par exemple, l'attribut `text` pour la balise `<h1>` : `<h1 text="Un titre"/>`).\
Un document **RML** est constitué de 2 balises parentes `<head>` et `<body>`.\
Exemple de la structure d'un fichier **RML** :
```rml
<head>
...
</head>
<body>
...
</body>
```
## En-tête
La balise jumelle `<head>` permet de définir l'en-tête d'un document **RML**, tel que son titre.\
Il est a noté que l'encodage par défaut, est l'UTF-8, et ne peut pas être changé.\
Actuellement, la seule balise enfant de la balise parente `<head>`, est la balise jumelle `<title>`, permettant de définir le titre d'un document **RML**.
```rml
<head>
    <title text="Document RML"/>
</head>
```
## Corp
La balise jumelle `<body>` permet de définir le corp d'un document **RML**, tel que du texte, des images, liens, etc.
### Balises génériques
Les balises suivantes permettent de définir différentes parties dans le document :
* `<header>` (balise jumelle) : permet de définir un **en-tête** dans `<body>`
* `<nav>` (balise jumelle) : permet de définir un menu de **navigation** dans `<body>`
* `<main>` (balise jumelle) : permet de définir un **contenu principal** dans `<body>`
* `<section>` (balise jumelle) : permet de définir un **contenu par thématique** dans `<main>`
* `<footer>` (balise jumelle) : permet de définir un **pied de page** dans `<body>`

Toutes ces balises (à l'exception de `<section>`) sont des enfants de la balise `<body>`.\
La balise `<section>` est une balise enfant de `<main>`.
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
   <header>...</header>
   <nav>...</nav>
   <main>
      <section>...</section>
      <section>...</section>
      <section>...</section>
   </main>
   <footer>...</footer>
</body>
```
### Texte
Il existe différentes balises pour afficher du texte :
* `<h1>` à `<h6>` (balise orpheline) : permet de définir les titres (du plus important `<h1>` au moins important `<h6>`)
* `<p>` (balise jumelle) : permet de définir les paragraphes
* `<span>` (balise jumelle) : permet de représenter du texte spécifique à l'intérieur d'une balise `<p>`

Les balises `<h1>` à `<h6>` possèdent l'attribut `text` (obligatoire), permettant de définir leurs contenus.\
Les balise `<p>` et `<span>` n'acceptent qu'en balise enfant que la balise `<span>`.
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
    <main>
        <section>
            <h1 text="Titre 1"/>
            <h2 text="Titre 2"/>
            <h3 text="Titre 3"/>
            <h4 text="Titre 4"/>
            <h5 text="Titre 5"/>
            <h6 text="Titre 6"/>
            <p>Un paragraphe</p>
            <p>Un paragraphe <span>avec du texte spécifique</span></p>
            <p>Un paragraphe <span>avec du texte spécifique<span>, et encore du texte spécifique</span></span></p>
        </section>
    </main>
</body>
```
### Lien
La balise orpheline `<link>` permet de définir un lien vers une ressource.\
Cette balise possède comme attributs :
* `target` (obligatoire) : contient le lien cible de la ressource
* `text` (optionnel) : permet de définir le texte du lien, si non définit, affiche le lien en tant que texte
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
    <main>
        <section>
            <link target="https://www.rust-lang.org/fr" text="Site officiel de Rust"/>
            <link target="https://www.rust-lang.org/fr"/>
        </section>
    </main>
</body>
```
### Image
La balise orpheline `<img>` permet de définir une image.\
Cette balise possède comme attributs :
* `src` (obligatoire) : permet de définir la source de l'image
* `text` (obligatoire) : permet de définir le texte dans le cas où l'image n'est pas chargée
* `target` (optionnel) : contient le lien cible de la ressource
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
    <main>
        <section>
            <img src="https://www.rust-lang.org/static/images/rust-social-wide.jpg" text="Une image de Rust" target="https://www.rust-lang.org/fr"/>
            <img src="https://www.rust-lang.org/static/images/rust-social-wide.jpg" text="Une image de Rust"/>
        </section>
    </main>
</body>
```
### Liste
La balise jumelle `<list>` permet de définir une liste.\
La balise `<list>` a pour balise enfant la balise jumelle `<item>`.\
La balise `<list>` possède l'attribut `type` (obligatoire), permettant de définir le type de liste, **numérotée** (`numbered`) ou **à puces** (`bulleted`).
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
    <main>
        <section>
            <list type="numbered">
                <item><p>Je suis une liste</p></item>
                <item><p>Je suis une liste</p></item>
                <item><p>Je suis une liste</p></item>
            </list>
            <list type="bulleted">
                <item><p>Je suis une liste</p></item>
                <item><p>Je suis une liste</p></item>
                <item><p>Je suis une liste</p></item>
            </list>
        </section>
    </main>
</body>
```
### Tableau
La balise jumelle `<table>` permet de définir un tableau.\
La balise `<table>` a pour balises enfant :
* `<thead>` (balise jumelle & obligatoire) : permet de définir l'en-tête des colonnes du tableau
* `<tbody>` (balise jumelle & obligatoire) : permet de définir l'ensemble des lignes du tableau
* `<tfoot>` (balise jumelle & optionnel) : permet de définir le résumer des colonnes du tableau

Les balises `<thead>`, `<tbody>` et `<tfoot>` ont pour balise enfant la balise jumelle `<line>`.\
La balise `<line>` a pour balise enfant la balise orpheline `<cell>`.\
La balise `<cell>` possède les attributs :
* `text` (obligatoire) : permet de définir le texte de la cellule
* `target` (optionnelle) : contient le lien cible de la ressource
* `cols` (optionnelle) : permet de définir le nombre de colonnes que va prendre la cellule, si non définit, la valeur par défaut est `1`
* `rows` (optionnelle) : permet de définir le nombre de lignes que va prendre la cellule, si non définit, la valeur par défaut est `1`
```rml
<head>
    <title text="Document RML"/>
</head>
<body>
    <main>
        <section>
            <table>
                <thead>
                    <line>
                    <cell text="Colonne 1"/>
                    <cell text="Colonne 2"/>
                    <cell text="Colonne 3"/>
                    <cell text="Colonne 4"/>
                    </line>
                </thead>
                <tbody>
                    <line>
                        <cell text="Cellule 1"/>
                        <cell text="Cellule 2"/>
                        <cell text="Cellule 3"/>
                        <cell text="Cellule 4"/>
                    </line>
                    <line>
                        <cell cols="2" text="Cellule 5"/>
                        <cell rows="2" text="Cellule 7"/>
                        <cell text="Cellule 8"/>
                    </line>
                    <line>
                        <cell cols="4" target="https://www.rust-lang.org/fr" text="Cellule lien"/>
                    </line>
                </tbody>
                <tfoot>
                    <line>
                        <cell text="Colonne 1"/>
                        <cell text="Colonne 2"/>
                        <cell text="Colonne 3"/>
                        <cell text="Colonne 4"/>
                    </line>
                </tfoot>
           </table>
        </section>
    </main>
</body>
```
## Attributs
Actuellement, seule les attributs suivants sont définies :
* `text` :
  * permet de définir du texte
  * s'applique sur les balises : `h1` à `h6`, `link`, `img`, `cell`.
* `target` :
  * permet de définir le lien vers une ressource
  * s'applique sur les balises : `link`, `img`, `cell`
* `src` :
  * permet de définir la source de l'élément
  * s'applique sur les balises : `img`
* `type` :
  * permet de définir le comportement d'un élément
  * s'applique sur les balises : `list`
  * a pour valeur : `numbered`, `bulleted`
* `cols` :
  * permet de définir un nombre de colonnes pour une cellule
  * s'applique sur les balises : `cell`
* `rows` :
  * permet de définir un nombre de lignes pour une cellule
  * s'applique sur les balises : `cell`